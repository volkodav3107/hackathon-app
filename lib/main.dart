import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Named Routes Demo',
    // Start the app with the "/" named route. In this case, the app starts
    // on the FirstScreen widget.
    initialRoute: '/',
    routes: {
      // When navigating to the "/" route, build the FirstScreen widget.
      '/': (context) => FirstScreen(),
    },
  ));
}

class FirstScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Company Security'),
      ),
      body: Center(child: MainWidget()),
    );
  }
}

class MainWidget extends StatefulWidget {
  @override
  _MainWidgetState createState() => _MainWidgetState();
}

class _MainWidgetState extends State<MainWidget> {
  bool _isLocked = false;
  static const String _realPassword = "123456";

  void _lock() {
    setState(() {
      debugPrint("setState");
      debugPrint("$_isLocked");
      _isLocked = true;
    });
  }

  void _unlock(String password) {
//    if (password.contains(_realPassword)) {
    setState(() {
      _isLocked = false;
    });
//    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: [
        Container(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 50, 0, 0),
            child: Image.asset(
              _isLocked ? "images/secure.png" : "images/unsecure.png",
              height: 200,
              width: 200,
            ),
          ),
        ),
        Container(
          child: !_isLocked
              ? LockWidget(onPressed: () {
                  _lock();
                })
              : UnlockWidget(onPressed: () {
                  _unlock("123");
                }),
        ),
      ],
    );
  }
}

class LockWidget extends StatelessWidget {
  VoidCallback _onPressed;

  LockWidget({VoidCallback onPressed}) {
    this._onPressed = onPressed;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 150, 20, 0),
      child: RaisedButton(
        onPressed: () {
          // Navigate back to the first screen by popping the current route
          // off the stack.
          _onPressed();
        },
        child: Text('Заблокировать'),
      ),
    );
  }
}

class UnlockWidget extends StatelessWidget {
  VoidCallback _onPressed;

  UnlockWidget({VoidCallback onPressed}) {
    this._onPressed = onPressed;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 150, 20, 0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child: Text("Введите пароль"),
            ),
          ),
          Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(),
            ),
          ),
          Container(
              child: RaisedButton(
            onPressed: () {
              // Navigate back to the first screen by popping the current route
              // off the stack.
              _onPressed();
            },
            child: Text('Разблокировать'),
          ))
        ],
      ),
    );
  }
}
